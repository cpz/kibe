# kibe PWA

## Build Setup

``` bash
# install dependencies
npm i
# serve with hot reload at localhost:8080
npm run dev
# build for production with minification
npm run build
# generate /dist/
npm run generate
```

Stack:  
Nuxt <https://nuxtjs.org/guide/> c/ modulo  PWA (WebApp instalable) <https://pwa.nuxtjs.org/>

Todas las configuracion van en /nuxt.config.js  

Basicamente es vue, webpack + babel, con una buena organizacion,  
Service worker p/ cachear estaticos y requests, con push notifications integrado,

Framework UI: Buefy integrado con Bulma en reemplazo de bootstrap.  
(Probé varios y esa combinacion me parecio la mejor por liviana y customizable)
<https://buefy.github.io/documentation/start>  

Las hojas de estilo, fonts y estaticos para compilar en el bundle van en:
/assets/ y se hace import en
/nuxt.config.js ({ css: [] })

Para estaticos que no son para la vista inicial o externos, quizas combiene
linkearlos en el header, tambien en
/nuxt.config.js ({ head: { link:[] })

/statics/ se copia automaticamente a /dist/ al hacer el build,

Variables css en /src/assets/sass/_vars.sass'

El routeo se hace en base al directorio /pages/, se puede extender la configuracion
(<https://nuxtjs.org/guide/routing/)>  

Con Vuex es parecido, se cargan en /store/ (<https://nuxtjs.org/guide/vuex-store/)>

Para cachear en el service worker estaticos y requests, en /nuxt.config.js ({ workbox })

``` JSON
 runtimeCaching: [
  {
    urlPattern: 'https://cdn.materialdesignicons.com/.*',
    strategyOptions: {cacheableResponse: {statuses: [0, 200]}}
  },
]  
```

Para agregar funciones puntuales, agregar en /plugins/ y referenciar en nuxt.config.js
({ plugins: [] })  

```  JSON
  plugins: [  
    '@/plugins/funcName.js'
  ],  
```

Para usarlo como this.$funcName() en cualquier componente,  
por ej funcName.js seria algo asi:  

``` Javacript
import Vue from 'vue'  
Vue.prototype.$funcName = (url, options) => {  
  return ''  
}
```

Tools para optimizacion: tab "Audits" en chrome dev tools.  
(<https://developers.google.com/web/tools/lighthouse/),> ya viene por default creo

Viene tambien integrado con push notifications <https://pwa.nuxtjs.org/modules/onesignal>  
<https://onesignal.com/apps/1d27ae55-9fcd-435d-8d96-f313009f7f3a/notifications/new>  
acceso test -> kibedevs@gmail.com : kibe$123

Custom fonts <http://app.fontastic.me/#customize/KDt378yzjjDHRrjmo9ZDU6>  
(Para pasar iconos svg a fonts)  
acceso temporal -> kibedevs@gmail.com : kibe$123  
Descargar zip, renombrar styles.css -> icons.css y mover todo a /assets/  
