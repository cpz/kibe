const pkg = require('./package')
import { api } from './config'

module.exports = {
  // mode: 'universal', // para ssr
  mode: 'spa',

  // Headers of the page
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { hid: 'description', name: 'description', content: pkg.description },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' },
      { name: 'apple-mobile-web-app-status-bar-style', content: '#ffffff' }
    ],
    link: [
      // { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '/hind.css', defer: true },
      // { rel: 'stylesheet', href: '/icons.css', defer: true },
      { rel: 'stylesheet', href: '//file.myfontastic.com/KDt378yzjjDHRrjmo9ZDU6/icons.css', defer: true },
    ],
    script: [
      // { src: 'https://cdn.polyfill.io/v2/polyfill.min.js' },
    ]
  },
  env: {
    baseUrl: api
  },
  // Customize the progress-bar color
  loading: { color: '#ffffff' },
  // icon: {},

  // Global CSS
  css: [
    // '@/assets/hind.css',
    // '@/assets/icons.css',
    '@/assets/sass/_vars.sass',
    '@/assets/sass/main.sass',
    // "@/assets/buefy-vars.scss"
  ],

  // router: {
  //   middleware: 'i18n'
  // },
  // Plugins to load before mounting the App
  plugins: [
    // '@/plugins/ga.js',
    // '@/plugins/hotjar.js', 
    '@/plugins/i18n.js',
    '@/plugins/helpers.js', // funciones varias
    '@/plugins/cachedFetch.js', // localstorage cached fetchs
    { src: '@/plugins/persist.js', ssr: false }, // Vuex persist state
    { src: '@/plugins/slider.js', ssr: false }, // slider
    { src: '@/plugins/vue-cookie', ssr: false, injectAs: 'cookie'}
  ],
  // Nuxt.js modules
  modules: [,
    '@nuxtjs/pwa',
    ['@nuxtjs/moment', { locales: ['es', 'fr', 'it'], defaultLocale: 'en' }],
    ['nuxt-buefy', { // Doc: https://buefy.github.io/#/documentation
      // defaultTooltipType: 'is-info',
      defaultNoticeQueue: false,
      defaultSnackbarDuration: 1600
    }],
    // '@nuxtjs/onesignal',
    // 'axios'
  ],
  // oneSignal: { // push notifications
  //   init: {
  //     appId: '1d27ae55-9fcd-435d-8d96-f313009f7f3a',
  //     allowLocalhostAsSecureOrigin: true,
  //     autoRegister: false,
  //   }
  // },
  manifest: {
    name: 'Kibe'
  },
  // Build configuration
  build: {
    // vendor: [
    //   'babel-polyfill'
    // ],
    // extractCSS: true,
    // optimization: {
    //   splitChunks: {
    //     cacheGroups: {
    //       styles: {
    //         name: 'styles',
    //         test: /\.(css|vue)$/,
    //         chunks: 'all',
    //         enforce: true
    //       }
    //     }
    //   }
    // },
    // You can extend webpack config here
    extend(config, ctx) {
    }
  },
  workbox: {
    runtimeCaching: [
    {
      urlPattern: 'https://file.myfontastic.com/.*',
      handler: 'cacheFirst',
      strategyOptions: {
        cacheName: 'icons',
        cacheExpiration: {
          maxEntries: 10,
          maxAgeSeconds: 300
        },  
        cacheableResponse: { statuses: [0, 200] }
      }
    },
    {
      urlPattern: 'https://cdn.materialdesignicons.com/.*',
      handler: 'cacheFirst',
      method: 'GET',
      strategyOptions: {cacheableResponse: {statuses: [0, 200]}}
    },
    {
      urlPattern: 'https://fonts.gstatic.com/.*',
      handler: 'cacheFirst'
    },
    {
      urlPattern: '/*',
      handler: 'cacheFirst'
    },
    {
      urlPattern: 'https://fonts.googleapis.com/.*',
      handler: 'cacheFirst'
    }]
  },

  // render: {
  //   bundleRenderer: {
  //     shouldPreload: (file, type) => {
  //       return ['script', 'style', 'font'].includes(type)
  //     }
  //   }
  // },
}
