import Vue from 'vue'
 
Vue.prototype.$toFormData = (obj, form, namespace) => {
  let fd = form || new FormData();
  let formKey;

  for(let prop in obj) {
    fd.append(prop, obj[prop]);
  }
  return fd;
}
Vue.prototype.$hideParent = (e) => {
  e.path[1].classList.toggle('hide')
}