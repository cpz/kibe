import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

import * as en from '../locale/en.json';
import * as es from '../locale/es.json';
import * as fr from '../locale/fr.json';
import * as it from '../locale/it.json';

export default ({ app, store }) => {
  // Set i18n instance on app
  // This way we can use it in middleware and pages asyncData/fetch
  app.i18n = new VueI18n({
    locale: store.state.locale,
    fallbackLocale: 'en',
    messages: {
      // 'en': en,
      // 'es': es,
      // 'fr': fr,
      // 'it': it,
      'es': require('~/locale/es.json'),
      'en': require('~/locale/en.json'),
      'fr': require('~/locale/fr.json'),
      'it': require('~/locale/it.json')
    }
  });

  app.i18n.path = (link) => {
    if (app.i18n.locale === app.i18n.fallbackLocale) {
      return `/${link}`;
    }

    return `/${app.i18n.locale}/${link}`;
  }
}