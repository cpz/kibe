/*
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

export default () => {
  return new Vuex.Store({
    state: {
      search: 'x' // {}
    },
    mutations: {
      SET_SEARCH(state, search) {
        // merge data with previous state
        state.search = search // Object.assign({}, state.search, search)
      }
    },
    plugins: [createPersistedState()]
  })
}
// export default createStore 
*/

// import createPersistedState from 'vuex-persistedstate';
import { getField, updateField } from 'vuex-map-fields';

console.log('navigator.locale', window.navigator.language.split('-')[0])

export const state = () => ({
  search: {},
  flights: {},
  origin: '',
  destination: '',
  departure_date: null,
  return_date: null,
  return: '',
  token: '',
  step: '/',
  locales: [
    {
      code: 'en',
      name: 'en'
    },
    {
      code: 'es',
      name: 'es'
    },
    {
      code: 'fr',
      name: 'fr'
    },
    {
      code: 'it',
      name: 'it'
    }
  ],
  locale: window.navigator.language.split('-')[0]
});

export const getters = {
  getField,
}
export const mutations = {
  updateField,
  updateSearch (state, search) {
    state.search = search
  }
}
// export const plugins = [
//   createPersistedState()
// ]
