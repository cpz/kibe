importScripts('/_nuxt/workbox.4c4f5ca6.js')

workbox.precaching.precacheAndRoute([
  {
    "url": "/_nuxt/2c6ae2eeb2e93e367985.js",
    "revision": "faf056666f906d911e019cb84cdc0d67"
  },
  {
    "url": "/_nuxt/3120254c10029f6f4a42.js",
    "revision": "0b3199d6671544d21156c47311c1e881"
  },
  {
    "url": "/_nuxt/3866ea456003d666a592.js",
    "revision": "c0f5e5f31f4d8b36429992a8b02673ff"
  },
  {
    "url": "/_nuxt/8263612db1af2829397b.js",
    "revision": "917af34cedab86130030f82138fd76e2"
  },
  {
    "url": "/_nuxt/9ba7fb2c202dd308ecfd.js",
    "revision": "50ed39aecaf1d2ceb6ec28c6805a4eda"
  },
  {
    "url": "/_nuxt/a2b8c03e6859f37bff87.js",
    "revision": "533b8ce31bd46dea542b2e40957ee25f"
  },
  {
    "url": "/_nuxt/aac65dd4b8e4a15bcd3a.js",
    "revision": "6ff98160f12012310fb724aaccb98590"
  },
  {
    "url": "/_nuxt/c3a76804575d607e739c.js",
    "revision": "2d3f45031ae26002fb4223160098a745"
  }
], {
  "cacheId": "kibe",
  "directoryIndex": "/",
  "cleanUrls": false
})

workbox.clientsClaim()
workbox.skipWaiting()

workbox.routing.registerRoute(new RegExp('/_nuxt/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/.*'), workbox.strategies.networkFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('https://file.myfontastic.com/.*'), workbox.strategies.cacheFirst({"cacheName":"icons","cacheExpiration":{"maxEntries":10,"maxAgeSeconds":300},"cacheableResponse":{"statuses":[0,200]}}), 'GET')

workbox.routing.registerRoute(new RegExp('https://cdn.materialdesignicons.com/.*'), workbox.strategies.cacheFirst({"cacheableResponse":{"statuses":[0,200]}}), 'GET')

workbox.routing.registerRoute(new RegExp('https://fonts.gstatic.com/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('https://fonts.googleapis.com/.*'), workbox.strategies.cacheFirst({}), 'GET')
